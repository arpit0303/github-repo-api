package com.test.sigtuple.home;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.test.sigtuple.apiclient.GitHubService;
import com.test.sigtuple.db.AppDataBase;
import com.test.sigtuple.db.GitEntity;
import com.test.sigtuple.model.GitHub;
import com.test.sigtuple.model.HomeBean;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomePresenter implements HomeContract.Presenter {

    private HomeContract.View view;
    private HomeBean bean;

    HomePresenter(HomeContract.View view) {
        this.view = view;
        bean = new HomeBean();
    }

    @Override
    public void getGitIssues(final String org, final String repo) {
        Call<List<GitHub>> userGitList = GitHubService.getServices().openIssue(org, repo);

        userGitList.enqueue(new Callback<List<GitHub>>() {
            @Override
            public void onResponse(@NonNull Call<List<GitHub>> call, @NonNull Response<List<GitHub>> response) {
                if(response.code() == 200) {
                    List<GitHub> gitHubList = response.body();
                    bean.setOpenIssuesBean(gitHubList);
                    getCloseIssues(org, repo);
                }else {
                    view.getGitIssueFailure(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<GitHub>> call, @NonNull Throwable t) {
                view.getGitIssueFailure(t.getMessage());
            }
        });
    }

    private void getCloseIssues(String org, String repo) {
        Call<List<GitHub>> userGitList = GitHubService.getServices().closeIssue(org, repo);

        userGitList.enqueue(new Callback<List<GitHub>>() {
            @Override
            public void onResponse(@NonNull Call<List<GitHub>> call, @NonNull Response<List<GitHub>> response) {
                if(response.code() == 200) {
                    List<GitHub> gitHubList = response.body();
                    bean.setCloseIssuesBean(gitHubList);
                    insertDataInDB(bean);
                    view.getGitIssueSuccess(bean);
                }else {
                    view.getGitIssueFailure(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<GitHub>> call, @NonNull Throwable t) {
                view.getGitIssueFailure(t.getMessage());
            }
        });
    }

    private void insertDataInDB(final HomeBean bean) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                GitEntity gitEntity = new GitEntity();
                gitEntity.setOpenIssue(new Gson().toJson(bean.getOpenIssuesBean()));
                gitEntity.setCloseIssue(new Gson().toJson(bean.getCloseIssuesBean()));
                AppDataBase.getInstance(view.getContext()).gitDAO().insertAll(gitEntity);
            }
        }).start();
    }

    @Override
    public void getDataFromDB() {
        GitHandler handler = new GitHandler();
        new GitThread(handler).start();
    }


    class GitThread extends Thread {

        Handler handler;

        GitThread(Handler handler) {
            this.handler = handler;
        }

        @Override
        public void run() {
            super.run();
            GitEntity gitEntity = AppDataBase.getInstance(view.getContext()).gitDAO().getAll();
            Message message = handler.obtainMessage();
            Bundle bundle = new Bundle();
            if(gitEntity != null) {
                bundle.putString("openIssue", gitEntity.getOpenIssue());
                bundle.putString("closeIssue", gitEntity.getCloseIssue());
                message.setData(bundle);
            }else {
                message.setData(null);
            }

            handler.sendMessage(message);
        }
    }

    class GitHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();

            if (bundle != null && !bundle.isEmpty()) {
                Type listType = new TypeToken<List<GitHub>>() {
                }.getType();
                List<GitHub> openIssues = new Gson().fromJson(bundle.getString("openIssue"), listType);
                List<GitHub> closeIssues = new Gson().fromJson(bundle.getString("closeIssue"), listType);

                bean.setOpenIssuesBean(openIssues);
                bean.setCloseIssuesBean(closeIssues);

                view.getGitIssueSuccess(bean);
            }else {
                view.hideProgress();
            }
        }
    }
}
