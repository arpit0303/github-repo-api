package com.test.sigtuple.home;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.sigtuple.R;
import com.test.sigtuple.model.GitHub;
import com.test.sigtuple.model.HomeBean;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GitRecyclerAdapter extends RecyclerView.Adapter<GitRecyclerAdapter.ViewHolder> {

    private HomeBean bean;
    private Context context;

    GitRecyclerAdapter(HomeBean bean) {
        this.bean = bean;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.list_git, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position < bean.getOpenIssuesBean().size()) {
            GitHub openIssueBean = bean.getOpenIssuesBean().get(position);
            holder.tvPRNumber.setText(String.valueOf(openIssueBean.getNumber()));
            holder.tvTitle.setText(openIssueBean.getTitle());
            holder.tvUser.setText(openIssueBean.getUser().getLogin());
            holder.tvGitIssue.setText(context.getResources().getString(R.string.open));
            if (openIssueBean.getPullRequest() != null && openIssueBean.getPullRequest().getPatchUrl() != null) {
                holder.tvUrl.setText(openIssueBean.getPullRequest().getPatchUrl());
            } else {
                holder.tvUrl.setText(context.getResources().getString(R.string.na));
            }
        } else {
            int size = position - bean.getOpenIssuesBean().size();
            GitHub closeIssueBean = bean.getCloseIssuesBean().get(size);
            holder.tvPRNumber.setText(String.valueOf(closeIssueBean.getNumber()));
            holder.tvTitle.setText(closeIssueBean.getTitle());
            holder.tvUser.setText(closeIssueBean.getUser().getLogin());
            holder.tvGitIssue.setText(context.getResources().getString(R.string.close));
            if (closeIssueBean.getPullRequest() != null && closeIssueBean.getPullRequest().getPatchUrl() != null) {
                holder.tvUrl.setText(closeIssueBean.getPullRequest().getPatchUrl());
            } else {
                holder.tvUrl.setText(context.getResources().getString(R.string.na));
            }
        }
    }

    @Override
    public int getItemCount() {
        return (bean.getOpenIssuesBean().size() + bean.getCloseIssuesBean().size());
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_pr_number)
        TextView tvPRNumber;
        @BindView(R.id.tv_git_title)
        TextView tvTitle;
        @BindView(R.id.tv_user)
        TextView tvUser;
        @BindView(R.id.tv_url)
        TextView tvUrl;
        @BindView(R.id.tv_git_issue)
        TextView tvGitIssue;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
