package com.test.sigtuple.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.Toast;

import com.test.sigtuple.R;
import com.test.sigtuple.model.HomeBean;
import com.test.sigtuple.utils.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity implements HomeContract.View{

    @BindView(R.id.git_org)
    TextInputLayout gitOrg;

    @BindView(R.id.git_repo)
    TextInputLayout gitRepo;

    @BindView(R.id.submit)
    Button submit;

    @BindView(R.id.git_repo_details)
    RecyclerView rvGitRepo;

    HomeContract.Presenter presenter;
    ProgressDialog progressDialog;

    String orgName, repoName;
    HomeBean bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        presenter = new HomePresenter(this);
        progressDialog = new ProgressDialog(HomeActivity.this);
        progressDialog.setMessage(getResources().getString(R.string.loading_data));
        presenter.getDataFromDB();
    }

    @OnClick(R.id.submit)
    public void submitClicked() {
        orgName = gitOrg.getEditText().getText().toString();
        repoName = gitRepo.getEditText().getText().toString();
        if(AppUtils.isNotEmpty(orgName)) {
            gitOrg.setErrorEnabled(false);

            if(AppUtils.isNotEmpty(repoName)) {
                gitRepo.setErrorEnabled(false);
                progressDialog = new ProgressDialog(HomeActivity.this);
                progressDialog.setMessage(getResources().getString(R.string.loading_data));
                progressDialog.show();
                presenter.getGitIssues(orgName, repoName);

            }else {
                gitRepo.setErrorEnabled(true);
                gitRepo.setError(getResources().getString(R.string.empty_field));
            }
        }else {
            gitOrg.setErrorEnabled(true);
            gitOrg.setError(getResources().getString(R.string.empty_field));
        }
    }

    @Override
    public void getGitIssueSuccess(HomeBean bean) {
        progressDialog.dismiss();
        GitRecyclerAdapter adapter = new GitRecyclerAdapter(bean);
        rvGitRepo.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
        rvGitRepo.setAdapter(adapter);
    }

    @Override
    public void getGitIssueFailure(String errorMessage) {
        progressDialog.dismiss();
        Toast.makeText(HomeActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }


}
