package com.test.sigtuple.home;

import android.content.Context;

import com.test.sigtuple.model.HomeBean;

public interface HomeContract {

    interface View {

        void getGitIssueSuccess(HomeBean bean);

        void getGitIssueFailure(String errorMessage);

        Context getContext();

        void hideProgress();
    }

    interface Presenter {
        void getGitIssues(String org, String repo);

        void getDataFromDB();
    }
}
