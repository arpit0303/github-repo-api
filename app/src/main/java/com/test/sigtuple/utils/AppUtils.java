package com.test.sigtuple.utils;

/**
 * Created by Arpit on 12/01/19.
 */

public class AppUtils {

    public static boolean isNotEmpty(String str) {
        return str != null && !str.trim().isEmpty();
    }
}
