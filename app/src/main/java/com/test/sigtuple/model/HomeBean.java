package com.test.sigtuple.model;

import java.util.List;

public class HomeBean {
    private List<GitHub> openIssuesBean;
    private List<GitHub> closeIssuesBean;

    public List<GitHub> getOpenIssuesBean() {
        return openIssuesBean;
    }

    public void setOpenIssuesBean(List<GitHub> openIssuesBean) {
        this.openIssuesBean = openIssuesBean;
    }

    public List<GitHub> getCloseIssuesBean() {
        return closeIssuesBean;
    }

    public void setCloseIssuesBean(List<GitHub> closeIssuesBean) {
        this.closeIssuesBean = closeIssuesBean;
    }
}
