package com.test.sigtuple.apiclient;

import com.test.sigtuple.model.GitHub;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubInterface {

    @GET("repos/{org}/{repo}/issues?state=open")
    Call<List<GitHub>> openIssue(@Path("org") String org, @Path("repo") String repo);

    @GET("repos/{org}/{repo}/issues?state=closed")
    Call<List<GitHub>> closeIssue(@Path("org") String org, @Path("repo") String repo);
}
