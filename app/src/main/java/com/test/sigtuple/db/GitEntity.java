package com.test.sigtuple.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity
public class GitEntity {

    @PrimaryKey(autoGenerate = true)
    private int uid = 0;

    @ColumnInfo(name = "open_issue")
    private String openIssue;

    @ColumnInfo(name = "close_issue")
    private String closeIssue;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getOpenIssue() {
        return openIssue;
    }

    public void setOpenIssue(String openIssue) {
        this.openIssue = openIssue;
    }

    public String getCloseIssue() {
        return closeIssue;
    }

    public void setCloseIssue(String closeIssue) {
        this.closeIssue = closeIssue;
    }
}
