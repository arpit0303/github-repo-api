package com.test.sigtuple.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

@Dao
public interface GitDAO {

    @Query("SELECT * FROM gitEntity")
    GitEntity getAll();

    @Insert
    void insertAll(GitEntity gitEntity);

    @Delete
    void delete(GitEntity gitEntity);
}
