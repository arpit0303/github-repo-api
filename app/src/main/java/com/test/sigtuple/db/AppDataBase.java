package com.test.sigtuple.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;


@Database(entities = {GitEntity.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {

    public abstract GitDAO gitDAO();

    private static AppDataBase db;

    private static final String DATABASE_NAME = "git";

    public static AppDataBase getInstance(Context context) {
        if (db == null) {
            db = Room.databaseBuilder(context, AppDataBase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return db;
    }

    static final Migration FROM_1_TO_2 = new Migration(1, 2) {
        @Override
        public void migrate(final SupportSQLiteDatabase database) {
        }
    };
}
